# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :produto do
    nome "Livro de ruby"
    preco "9.99"
    quantidade 1
  end
end
