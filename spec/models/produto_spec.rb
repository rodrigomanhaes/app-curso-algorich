require 'spec_helper'

describe Produto do
  describe 'validações' do
    it { should_not have_valid(:nome).when(nil, '') }
    it { should_not have_valid(:quantidade).when(
      nil, '', 0, -1, 2.3, 'a') }
    it { should_not have_valid(:preco).when(0, -1, 0.99) }
  end
end