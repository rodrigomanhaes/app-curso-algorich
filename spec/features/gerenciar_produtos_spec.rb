require 'spec_helper'

feature 'gerenciar produtos' do
  scenario 'inclusão' do
    visit new_produto_path
    fill_in 'Nome', with: 'Playstation 4'
    fill_in 'Preço', with: '1240'
    fill_in 'Quantidade', with: '10'
    click_button 'Salvar'
    expect(page).to have_content 'Nome: Playstation 4'
    expect(page).to have_content 'Preço: R$ 1.240,00'
    expect(page).to have_content 'Quantidade: 10'
  end
end