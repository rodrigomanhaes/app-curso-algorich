class Produto < ActiveRecord::Base
  validates :nome, :quantidade, :preco, presence: true
  validates :quantidade, numericality: {
    greater_than: 0, only_integer: true }
  validates :preco, numericality: {
    greater_than_or_equal_to: 1 }
end
