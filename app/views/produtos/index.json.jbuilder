json.array!(@produtos) do |produto|
  json.extract! produto, :nome, :preco, :quantidade
  json.url produto_url(produto, format: :json)
end
