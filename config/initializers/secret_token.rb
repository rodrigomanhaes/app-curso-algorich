# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Livraria::Application.config.secret_key_base = '7c6afbe41dc893dddee1e66e3ef320e947f2b4d750a859f2124864305c28ecbb65a6dfd86d3cfad99c6ebec48c539ab19c549b9ba6aa128a5d7460c6f6a0dbb1'
